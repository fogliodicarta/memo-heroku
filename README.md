# Documentazione Memo
## Progetto
### Librerie utilizzate
* Bulma versione 0.8 (css)
* FontAwesome5 (js)
* JQuery versione 3.4.1 (js)
* JQueryToast (sia js che css)

Le librerie `Bulma` e `FontAwesome5` sono state utilizzate per la parte grafica del progetto: icone, layout della pagina, navbar ecc...

`JQuery` è stata utilizzata per la gestione del DOM, le richieste ajax al server e per alcune animazioni

`JQueryToast` per le notifiche di save e delete (Animazione e grafica)


## Moduli di node utilizzati
* `express` 4.17.1
* `ejs` 3.0.1
* `fs`

`Express` è stato utilizzato per la creazione del server, la gestione delle richieste HTTP e per la logica applicativa

`Ejs` è stato utilizzato come render engine per la creazione dei memo

`Fs` è stato usato per salvare il file JSON contenente i dati dei memo

## Altre dependencies
* `Heroku`
* `Google Assistant`
* `IFTTT`







<img src="https://i.imgur.com/DDsJZYv.png" alt="memo" width="">