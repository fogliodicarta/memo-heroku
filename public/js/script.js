let bigquadrato//container dei memo
let creabtn//non cambia è sempre il crea nella navbar
let barra//non cambia è la barra per inserire i titoli dei memo

let aggiornabtn//cambia ogni volta che viene cliccato save

let deletebtn//cambia ogni volta che si clicca sulla x
$(window).on('load', () => {
    console.log("onload")
    creabtn = $('#creabtn')
    barra= $('#barra')
    bigquadrato = $('#bigquadrato')

    ///////////////////////////////////////////////////////////
    //LISTENER//
    aggiungiListeners();
    /////////////////////////////////////////////////////////////
});
function crea(titolomemo){
    $.ajax({
        url: '/creamemo',
        type: 'POST',
        data:{
            nome:titolomemo
        },
        success: function (data) {
            console.log("POST ANDATO");
            nm=data.index
            titolomemo=data.title
            divquadrato=`<div class="has-background-warning quadrato">
            <div class="sopramemo">
                <p> <strong>${titolomemo}</strong>
                    <button id="deletebtn${nm}"class="bottonedelete delete has-background-danger"> X </button>
                    <button id="aggiorna${nm}" class="bottoneaggiorna button has-background-info"> <i class="fas fa-save"></i></button>
                </p>
            </div>

            <div class="sottomemo">
                <textarea name="testomemo" id="testomemo${nm}" class="testomemo"
                    placeholder="Inserisci testo" spellcheck="false"></textarea>
            </div>
    </div>`;
            bigquadrato.append(divquadrato);
            $($('.quadrato')[nm]).css("display","none")
            //$($('.quadrato')).fadeIn(600);
            $($(".quadrato")[nm]).animate({width:"show",height:"100px"});
            $($(".quadrato")[nm]).animate({width:"show",height:"300px"});
            aggiungiListeners();
        },
        error:()=>{
            console.log("non va post");
        }
    });
}
function aggiorna(idmemo,txtmemo) {
    $.ajax({
        url: '/aggiornamemo',
        type: 'PUT',
        data:{
            indicememo:idmemo,
            testomemo:txtmemo
        },
        success: function (data) {
            console.log("PUT ANDATO");
            $.toast({
                heading: 'Salvato',
                text: `Il tuo memo numero ${data.index} (${data.title}) è stato salvato con successo`,
                icon: 'success',
                loader: true,       
                loaderBg: '#9EC600', 
                hideAfter: 5000  
            })
        },
        error:()=>{
            console.log("non va put");
        }
    });
}

function cancella(idmemo){
    $.ajax({
        url: '/deletememo?id='+idmemo.toString(),
        type: 'DELETE',
        success: function (data) {
            console.log("DELETE ANDATO");
            idcancellato = data.index;
            for(i=idcancellato+1;i<$(".quadrato").length;i++){
                console.log("id",i)
                $("#deletebtn"+i.toString()).prop("id","deletebtn"+(i-1).toString());
                $("#aggiorna"+i.toString()).prop("id","aggiorna"+(i-1).toString());
                $("#testomemo"+i.toString()).prop("id","testomemo"+(i-1).toString());
            }
            $($(".quadrato")[idcancellato]).animate({width:"hide",height:"hide"},{complete:()=>{
                $(".quadrato")[idcancellato].remove();
            }});
            $.toast({
                heading: 'Eliminato',
                text: `Attenzione! Il tuo memo numero ${data.index} (${data.title}) è stato eliminato con successo`,
                icon: 'warning',
                loader: true,       
                loaderBg: '#9EC600', 
                hideAfter: 5000  
            })
        },
        error:()=>{
            console.log("non va delete");
        }
    });
}

function aggiungiListeners(){
    $(document).add('*').off();//rimuove tutti gli event handler così non si duplica
    $(".navbar-burger").click(function () {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    });
    creabtn.click(()=>{//post
        crea(barra.val());
    });
    $('.bottoneaggiorna').click((e)=>{//put
        aggiornabtn= $(e.target);
        idmemo=aggiornabtn[0].id.substring(8);
        txtmemo=$('#testomemo'+ idmemo).val();
        aggiorna(idmemo,txtmemo);
    });
    $('.bottonedelete').click((e)=>{//delete
        deletebtn= $(e.target);
        idmemo=deletebtn[0].id.substring(9);
        cancella(idmemo);
    });
}
